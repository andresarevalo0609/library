# -*- coding: utf-8 -*-
{
    'name': "Library",
    'summary': "library, Collection of information resources, in print or in other forms, that is organized and made accessible for reading or study.",
    'description': "A library is a place where literary or reference materials (as books, manuscripts, recordings, or films) are kept for use but are not for sale.",
    'author': "Andres",
    'website': "none",
    'category': 'test',
    'version': '0.1',
    'depends': ['base'],
    'data': [],
}
